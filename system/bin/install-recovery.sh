#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/bootdevice/by-name/recovery:24664976:cee227b9dfa580d93c113be15bef4450c4d0c9e4; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/platform/bootdevice/by-name/boot:11666320:5b3d9a7458f6407804c2f7ba0474616115427474 EMMC:/dev/block/platform/bootdevice/by-name/recovery cee227b9dfa580d93c113be15bef4450c4d0c9e4 24664976 5b3d9a7458f6407804c2f7ba0474616115427474:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
